//
//  ContentView.swift
//  FlowersClassifier
//
//  Created by rifqi triginandri on 22/06/24.
//

import SwiftUI
import CoreML

struct ContentView: View {
    @State private var classificationLabel: String = ""
    @State private var selectedImage: UIImage? = nil
    @State private var isImagePickerPresented = false
    
    private var model: FlowerClassifierModel?
    
    init() {
        do {
            model = try FlowerClassifierModel(configuration: MLModelConfiguration())
        } catch {
            print("Failed to initialize model: \(error.localizedDescription)")
        }
    }
    
    var body: some View {
        VStack {
            Spacer()
            if let image = selectedImage {
                Image(uiImage: image)
                    .resizable()
                    .frame(width: 300, height: 300)
                    .scaledToFit()
            } else {
                Text("Select an Image")
                    .frame(width: 300, height: 300)
                    .background(Color.gray.opacity(0.2))
            }

            Button("Select Image") {
                self.isImagePickerPresented = true
            }
            .padding()
            .foregroundColor(Color.white)
            .background(Color.blue)
            .sheet(isPresented: $isImagePickerPresented) {
                ImagePicker(selectedImage: self.$selectedImage)
            }

            if selectedImage != nil {
                Button("Classify") {
                    classifyImage()
                }
                .padding()
                .foregroundColor(Color.white)
                .background(Color.green)
            }

            Text(classificationLabel)
                .padding()
                .font(.body)
            
            Spacer()
        }
        .padding()
    }
    
    private func classifyImage() {
        
        guard let model = model,
              let image = selectedImage,
              let resizedImage = image.resizeImageTo(size: CGSize(width: 299, height: 299)),
              let buffer = resizedImage.convertToBuffer() else {
            
            self.classificationLabel = "Failed to process image"
            return
        }
        
        do {
            let output = try model.prediction(image: buffer)
            let results = output.targetProbability.sorted { $0.1 > $1.1 }
            let result = results.map { (key, value) in
                return "\(key) = \(String(format: "%.2f", value * 100))%"
            }.joined(separator: "\n")
            
            self.classificationLabel = result
        } catch {
            self.classificationLabel = "Failed to classify image"
            print("Error during classification: \(error.localizedDescription)")
        }
    }
    

}

#Preview {
    ContentView()
}
