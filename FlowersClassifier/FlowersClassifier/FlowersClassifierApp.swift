//
//  FlowersClassifierApp.swift
//  FlowersClassifier
//
//  Created by rifqi triginandri on 22/06/24.
//

import SwiftUI

@main
struct FlowersClassifierApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
