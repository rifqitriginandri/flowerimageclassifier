//
//  UIImage+Extension.swift
//  FlowersClassifier
//
//  Created by rifqi triginandri on 23/06/24.
//

import UIKit

extension UIImage {
    
    // Resize image input fit image model ml
    func resizeImageTo(size: CGSize) -> UIImage? {
        UIGraphicsBeginImageContext(size)
        draw(in: CGRect(origin: .zero, size: size))
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return resizedImage
    }

    //Convert UIImage to CVPixelBuffer for model ml predict
    func convertToBuffer() -> CVPixelBuffer? {
        guard let cgImage = self.cgImage else {
            print("Failed to get CGImage")
            return nil
        }

        let frameSize = CGSize(width: cgImage.width, height: cgImage.height)
        var pixelBuffer: CVPixelBuffer? = nil
        let options: [NSObject: AnyObject] = [
            kCVPixelBufferCGImageCompatibilityKey: true as AnyObject,
            kCVPixelBufferCGBitmapContextCompatibilityKey: true as AnyObject
        ]

        let status = CVPixelBufferCreate(kCFAllocatorDefault, Int(frameSize.width), Int(frameSize.height), kCVPixelFormatType_32ARGB, options as CFDictionary, &pixelBuffer)

        guard status == kCVReturnSuccess, let buffer = pixelBuffer else {
            print("Failed to create CVPixelBuffer")
            return nil
        }

        CVPixelBufferLockBaseAddress(buffer, .readOnly)
        let context = CGContext(data: CVPixelBufferGetBaseAddress(buffer),
                                width: Int(frameSize.width),
                                height: Int(frameSize.height),
                                bitsPerComponent: 8,
                                bytesPerRow: CVPixelBufferGetBytesPerRow(buffer),
                                space: CGColorSpaceCreateDeviceRGB(),
                                bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue)

        guard let drawingContext = context else {
            print("Failed to create CGContext")
            CVPixelBufferUnlockBaseAddress(buffer, .readOnly)
            return nil
        }

        drawingContext.draw(cgImage, in: CGRect(origin: .zero, size: frameSize))
        CVPixelBufferUnlockBaseAddress(buffer, .readOnly)

        return buffer
    }
}
